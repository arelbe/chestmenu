Original library developed by Zoutelande on GitHub
https://github.com/Zoutelande/ChestGuiAPI

This version adds many extended functions to the API.
Some changes to the base are made to better suit the needs of developer of this project.
