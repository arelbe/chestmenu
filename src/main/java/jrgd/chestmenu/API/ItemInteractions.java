package jrgd.chestmenu.API;

import jrgd.chestmenu.Types.InventorySummary;
import net.minecraft.entity.player.PlayerInventory;
import net.minecraft.item.ItemStack;
import net.minecraft.item.Items;
import net.minecraft.nbt.*;
import net.minecraft.server.network.ServerPlayerEntity;
import net.minecraft.text.Text;

import java.util.ArrayList;
import java.util.List;

public class ItemInteractions {
    public static boolean isSimilar (ItemStack a, ItemStack b) {
        boolean check = false;

        switch (((a.hasTag()) ? 1 : 0) + ((b.hasTag()) ? 1 : 0)) {
            case 0:
                check = a.isItemEqual(b);
                break;
            case 1:
                break;
            case 2:
                System.out.println("I want my tags!");
                // check = (a.isItemEqual(b) && a.getTag().hashCode() == b.getTag().hashCode());
                break;
            default:
                break;
        }
        return check;
    }

    public static ItemStack setItemLore(ItemStack input, List<Text> lore) {
        CompoundTag compoundTag = input.getOrCreateSubTag("display");
        ListTag loreTag = new ListTag();
        if (lore != null) {
            for (Text line : lore) {
                loreTag.add(StringTag.of(Text.Serializer.toJson(line)));
            }
            compoundTag.put("Lore",loreTag);
        }
        else {
            compoundTag.remove("Lore");
        }
        return input;
    }

    public static ItemStack customPlayerHead(String url, int uuid0, int uuid1, int uuid2, int uuid3, int count) {
        ItemStack customHead = new ItemStack(Items.PLAYER_HEAD, count);
        CompoundTag textures = new CompoundTag();
        ListTag texturesList = new ListTag();
        IntArrayTag UUIDParts = new IntArrayTag(new int[] {uuid0, uuid1, uuid2, uuid3});
        CompoundTag Properties = new CompoundTag();
        texturesList.add(textures);
        textures.putString("Value", url);

        Properties.put("textures", texturesList);
        CompoundTag SkullOwner = customHead.getOrCreateSubTag("SkullOwner");
        SkullOwner.put("Id", UUIDParts);
        SkullOwner.put("Properties", Properties);

        return customHead;
    }

    public static InventorySummary inventorySummary(PlayerInventory inv, ItemStack item) {
        int count = 0;
        ArrayList<Integer> output = new ArrayList<>();
        for(int index = 0; index < inv.size(); index++) {
            ItemStack comp = inv.getStack(index);

            switch (((comp.hasTag()) ? 1 : 0) + ((item.hasTag()) ? 1 : 0)) {
                case 0:
                    if (comp.isItemEqual(item)) {
                        count += comp.getCount();
                        output.add(index);
                    }
                    break;
                case 1:
                    break;
                case 2:
                    if ((comp.isItemEqual(item) && comp.getTag().hashCode() == item.getTag().hashCode())) {
                        count += comp.getCount();
                        output.add(index);
                    }
                    break;
                default:
                    break;
            }
        }
        return new InventorySummary(count, output);
    }

    // Item Trades (currently handles single-type, single-type
    // Plans to add lists of items exists

    public static void trade(ServerPlayerEntity playerEntity, ItemStack input, ItemStack output) {
        InventorySummary summary = inventorySummary(playerEntity.inventory, input);
        if (summary.getAmount() >= input.getCount()) {

            int amount = input.getCount();
            int slot;
            while (amount > 0) {
                slot = summary.getSlots().get(0);
                if (playerEntity.inventory.getStack(slot).getCount() >= amount) {
                    playerEntity.inventory.removeStack(slot, amount);
                    amount = 0;
                }
                else {
                    amount -= playerEntity.inventory.getStack(slot).getCount();
                    playerEntity.inventory.removeStack(slot);
                }
                summary.getSlots().remove(0);
            }
            playerEntity.inventory.offerOrDrop(playerEntity.world, output);
        }
    }

    public static void fullTrade(ServerPlayerEntity playerEntity, ItemStack input, ItemStack output) {
        InventorySummary summary = inventorySummary(playerEntity.inventory, input);
        if (summary.getAmount() >= input.getCount()) {

            int amount = input.getCount();
            int tradeAmount = (summary.getAmount() / amount);
            amount *= tradeAmount;

            int max = input.getMaxCount();
            int slot = -1;

            while (amount > 0) {
                slot = summary.getSlots().get(0);
                if (playerEntity.inventory.getStack(slot).getCount() >= amount) {
                    playerEntity.inventory.removeStack(slot, amount);
                    amount = 0;
                }
                else {
                    amount -= playerEntity.inventory.getStack(slot).getCount();
                    playerEntity.inventory.removeStack(slot);
                }
                summary.getSlots().remove(0);
            }
            output.setCount(output.getCount() * tradeAmount);
            playerEntity.inventory.offerOrDrop(playerEntity.world, output);


        }
    }
}
