package jrgd.chestmenu.Types;

import java.util.ArrayList;

public class InventorySummary {
    private int amount = 0;
    private ArrayList<Integer> slots;

    public InventorySummary(int Amount, ArrayList<Integer> Slots) {
        this.amount = Amount;
        this.slots = Slots;
    }

    public boolean Exists() {
        if (amount > 0)
            return true;
        else
            return false;
    }

    public int getAmount() {
        return amount;
    }

    public ArrayList<Integer> getSlots() {
        return slots;
    }
}
